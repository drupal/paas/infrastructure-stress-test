# Base Python Version
FROM python:3.6

WORKDIR /usr/src/app

COPY requirements.txt .
COPY locustfile.py .

RUN pip3 install -r requirements.txt 

CMD command locust -f locustfile.py -u $NUMBER_USERS -r $SPAWN_RATE --run-time $RUN_TIME --csv $FILE_NAME --headless --host NotUsed> $FILE_NAME.logs 2>&1
