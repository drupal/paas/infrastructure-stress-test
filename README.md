
# Custom tool to Stress test a website

This package is based on [locust](locust.io) with a simple webcrawler embeded.   
The purpose is to:  

    1. Retrieve all the URLs belonging to a base URL.      
    2. Randomly access one of the URLs of the list generated in `1.`   
    3. At any point of interest, stop the program (Or define a timeout)

The generated report is done in `.csv`


## Example usage by pod deployment:

1. Make sure you have the `PVC` in the cluster, if you dont, run: `kubectl apply -f resources/pvc.yaml`
2. Populate a file (example:`listOfSites`) with the targeted `urls`
3. Run the following command:   
`./generatePods.sh -f listOfSites  -u $(Number of Users) -s $(User Spawn Rate) -n $(Number of Pods per Site) -r $(Running Time)`    
4. Wait for the pods to stop to retrieve the data , using `kubectl cp
   pvc-access:/usr/src/app/data/ $DESTIONATION_FOLDER`
Note: on step `3` make sure to `chmod` the `generatePods` in order to be executable  


Example run command for one user per pod, with 1 pod per site, for 
1h30minutes run: `./generatePods.sh -f listOfSites -u 1 -s 1 -n 1 -r 1h30m`

## Example manual usage:

Run the following commands:
```
URL=$URL_TO_TEST
export URL
locust -f locustfile.py
```

Then access `http://localhost:8089/`, select the number of virtual users, spawn rate and host.

## Example manual usage with terminal only:

```
URL=$URL_TO_TEST
export URL
locust -f locustfile.py  -u $NUMBER_OF_USERS -r $SPAWN_RATE --csv $NAME_OF_FILE --headless --host NotUsed
```

--- 

# Data Analysis

The following section presents the procedure done to retrieve data, followed by
a brief explanation on the collected data, as well a sample for graphical
representation.

## Stress Run Procedure 
For the old infrastructure:
1. Select the list of sites to be tested
2. Run stress tests on each


For the new infrastructure:
The following steps were made for each run
1. Edit the `PHP` `configmap`, where one of the following is changed:
  `pm.max_children`, `pm.start_servers`, `pm.min_spare_servers`,
  `pm.max_servers`
2. Access the PHP container, executing `oc debug pod/drupal-deployment-$DEPLOYMENT_NAME -c=php-fpm`, then run `drush cr` (This will rebuild cache)
3. Start the stress test pods on a dedicated cluster, running
   `./generatePods.sh -f $FILE_WITH_WEBSITE_URLS -u $NUMBER_OF_USERS -s
   $SPAWN_RATE -r $RUN_TIME -n $NUMBER_OF_CONCURRENT_PODS`
4. While the stress is being done, monitor resource consumption on `okd
   console`, for drupal staging being the
   [following](console-openshift-console.drupal-stg.cern.ch])
5. Once all the pods have finished, copy the data from the `PV` to your local
   machine, by running `kubectl cp pvc-access:/usr/src/app/data .` (Note: If `pvc` pod is not running, you can run it with `kubectl apply -f resources/pvcMountedPod.yaml`)



