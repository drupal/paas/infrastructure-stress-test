import sys, glob, pathlib, os, time, datetime
import pandas as pd # for data manipulation
import plotly.express as px # for data visualization
import numpy as np # for data manipulation
import plotly.graph_objects as go # for data visualization
import statsmodels.api as sm # to build a LOWESS model
from plotly.subplots import make_subplots
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

if len(sys.argv)<2:
    print("Please provide CSV Number")
    exit(1)
csvNumber = sys.argv[1]
print("CSV Number: ", csvNumber)
path=pathlib.Path().absolute()
# Get all files with specified number
all_files = glob.glob(os.path.join(path, "data/stress-test-"+csvNumber+"*history.csv"))

# First retrieve the minTimeStamp to have the starting point in time
minTimeStamp = time.time()
for f in all_files:
    df = pd.read_csv(f,sep=',')
    if df['Timestamp'].min()<minTimeStamp:
        minTimeStamp = df['Timestamp'].min()

print("Absolute timestamp:", minTimeStamp)
# Read all files and create DataFrame with all file's data and with relative timestamp
avgTimeFunctions =[]
# Sum of users functions
userSumFunctions = []
# maxRelativeTimeStamp will be the upper limit on the time plot(X axis)
maxRelativeTimeStamp = 0
for f in all_files:
    df = pd.read_csv(f, sep=',')
    mid = f.split('/')[-1]
    df['file'] = mid.split('_')[0]
    df['relative_timestamp'] = df['Timestamp'] - minTimeStamp
    # Convert Timestamp diff into seconds
    df['relative_timestamp'].apply(lambda x : datetime.timedelta(seconds=x))
    if df['relative_timestamp'].max() > maxRelativeTimeStamp:
        maxRelativeTimeStamp=df['relative_timestamp'].max()
    # Make interpolation of given file
    fillVal=(df['Total Average Response Time'].iloc[1],df['Total Average Response Time'].iloc[-1])
    avgTimeInterpolationFunction = interp1d(df['relative_timestamp'].to_numpy(),df['Total Average Response Time'].to_numpy(), kind="linear",fill_value=fillVal, bounds_error=False)
    fillUserVal=(df['User Count'].iloc[1],df['User Count'].iloc[-1])
    userInterpolationFunction = interp1d(df['relative_timestamp'].to_numpy(),df['User Count'].to_numpy(), kind="linear",fill_value=fillUserVal, bounds_error=False)
    avgTimeFunctions.append(avgTimeInterpolationFunction)
    userSumFunctions.append(userInterpolationFunction)


# avg_of_functions will make an average Y value for a given X based on all the interpolated functions of the retrieved data
def avg_of_functions(x, functionList):
    sumYValue = 0
    for singleFunction in functionList:
        sumYValue += singleFunction(x)
    return sumYValue/len(functionList)

# avg_of_functions will make an average Y value for a given X based on all the interpolated functions of the retrieved data
def sum_of_functions(x, functionList):
    sumYValue = 0
    for singleFunction in functionList:
        sumYValue += singleFunction(x)
    return sumYValue

# data will be a generated dataframe with average of Interpolated functions
data = [None] * maxRelativeTimeStamp
for i in range (0,maxRelativeTimeStamp):
    data[i] = [i,avg_of_functions(i,avgTimeFunctions),sum_of_functions(i,userSumFunctions)]

df = pd.DataFrame(data)

file = open(csvNumber+"-plotline", "wb")
np.save(file, df)



### Just giving a preview, can be deleted from here on:

file = open(csvNumber+"-plotline", "rb")
#read the file to numpy array
finalPlotCoordinates = np.load(file, allow_pickle=True)

x = [x1[0] for x1 in finalPlotCoordinates]
y = [y1[1] for y1 in finalPlotCoordinates]
yUsers = [y2[2] for y2 in finalPlotCoordinates]
trace1 = go.Scatter(x=x,y=y, mode="lines")
trace2 = go.Scatter(x=x,y=yUsers, mode="lines")
fig = make_subplots(rows=1, cols=2)
fig.update_xaxes(title_text="Time (seconds)")
fig.update_yaxes(title_text="AVG Response time (miliseconds)", row=1, col=1)
fig.update_yaxes(title_text="Number of Active Users", row=1, col=2)
fig.update_xaxes(type='category') # Gives more fine-grained values on X axis
fig.add_trace(trace1, row=1, col=1)
fig.add_trace(trace2, row=1, col=2)
fig.show()










































# Draw a interpolated line
#plt.plot(xnew, f1(xnew), '-')
# ------- LOWESS -------
# Generate y_hat values using lowess, try a couple values for hyperparameters
#generatedCoordinates = sm.nonparametric.lowess(df['Total Average Response Time'], df['relative_timestamp'] , frac=0.1) # note, default frac=2/3
# Save lowess graph into file
#file = open(csvNumber+"-plotline", "wb")
#np.save(file, generatedCoordinates)
# open the file in read binary mode
#file = open(csvNumber+"-plotline", "rb")

#read the file to numpy array
#finalPlotCoordinates = np.load(file)
#x = [x1[0] for x1 in finalPlotCoordinates]
#y = [y1[1] for y1 in finalPlotCoordinates]
# ---------END OF LOWESS -----

#for f in functions:
#
#    # Show Plot with all data for reference
#    trace1 = go.Scatter(x=x,y=y, mode="lines")
#    #trace2 = go.Scatter(x = df['relative_timestamp'], y = df['Total Average Response Time'], mode="markers" )
#    fig = make_subplots(rows=1, cols=1)
#    fig.add_trace(trace1, row=1, col=1)
#    #fig.add_trace(trace2, row=1, col=1 )
#
#fig.update_layout(xaxis=dict(tickangle=90))
#fig.show()
