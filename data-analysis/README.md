

# Data Analysis

These python scripts objective is to gather information generated accross
multiple runs and converge into final plot to represent average results of such run.   

`csv-analyzer.py` will receive as an argument the number of the run (example:
execution `python3 csv-analyzer.py 29285`, where `29285` represents the run
that generated multiple `29285_stats_history_csv`), and will generate a
`29285-plotline` binary file containing the resulting average interpolation
function.

`merge-plots.py` will receive as an argument from one files to N, each containing the
average interpolation function of a run, and will generate a plot with all of
them.


