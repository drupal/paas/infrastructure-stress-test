import sys, glob, pathlib, os
import numpy as np # for data manipulation
import plotly.graph_objects as go # for data visualization
from plotly.subplots import make_subplots


if len(sys.argv)<2:
    print("Please provide at least a file name")
    exit(1)
fileList =[]
print
for i in range (1,len(sys.argv)):
    fileList.append(sys.argv[i])

print("File Names: ", fileList)
fig = make_subplots(rows=1, cols=2)
# open the file in read binary mode
for fileName in fileList:
    file = open(fileName,"rb")
    #read first file values
    finalPlotCoordinates = np.load(file)
    x = [x1[0] for x1 in finalPlotCoordinates]
    y = [y1[1] for y1 in finalPlotCoordinates]
    y2 = [y2[2] for y2 in finalPlotCoordinates]
    trace = go.Scatter(x=x,y=y, mode="lines", name=fileName)
    trace2 = go.Scatter(x=x,y=y2, mode="lines", name=fileName)
    fig.add_trace(trace, row=1, col=1)
    fig.add_trace(trace2, row=1, col=2)

fig.update_xaxes(title_text="Time (seconds)", title_font_size=25)
fig.update_yaxes(title_text="AVG Response time (miliseconds)", row=1, col=1, title_font_size=25)
fig.update_yaxes(title_text="Number of Active Users", row=1, col=2, title_font_size=25)
fig.update_xaxes(type='category') # Gives more fine-grained values on X axis
fig.update_layout(xaxis=dict(tickangle=90))
fig.show()

