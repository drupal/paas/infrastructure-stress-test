#!/bin/bash
usage() { echo "Usage: $0 [--users <NUMBER_USERS>] [--spawn-rate <SPAWN_RATE>][--run-time <RUN_TIME>][--file <FILE>] [--number <N_REPLICAS>]"1>&2; exit 1; }

# Options

ARGS=$(getopt -o 'u:s:f:n:r:' --long 'users:,spawn-rate:,file:,number:,run:' -- "$@") || exit 1

eval "set -- $ARGS"

while true; do
  case "$1" in
    (-u|--users)
      NUMBER_USERS="$2"; shift 2;;
    (-s|--spawn-rate)
      SPAWN_RATE="$2"; shift 2;;
    (-f|--file)
      FILE_WITH_LIST="$2"; shift 2;;
    (-n|--number)
      N_REPLICAS="$2"; shift 2;;
    (-r|--run-time)
      RUN_TIME="$2"; shift 2;;
    (--) shift; break;;
    (*) usage;;
  esac
done
if [[ -z $FILE_WITH_LIST ]]; then
        echo "missing file list"
        usage
fi
if [[ -z $NUMBER_USERS ]]; then
        echo "missing number of users"
        usage
fi
if [[ -z $SPAWN_RATE ]]; then
        echo "missing spawn rate"
        usage
fi
if [[ -z $RUN_TIME ]]; then
        echo "missing run time"
        usage
fi
if [[ -z $N_REPLICAS ]]; then
    N_REPLICAS=1
fi

export RUN_TIME
export FILE_WITH_LIST
export SPAWN_RATE
export NUMBER_USERS

export BASE_NAME=stress-test-${RANDOM}
for ((i=0;i<N_REPLICAS;i++));do
    VAL=1
    while read line; do
        URL=$line
        export VAL
        export URL
        NAME=$BASE_NAME-$VAL-$i
        FILE_NAME=/usr/src/app/data/$BASE_NAME-$VAL-$i
        export NAME
        export FILE_NAME
        echo "Creating Pod for" $URL
        envsubst < resources/newStressTestPod.yaml | kubectl create -f -
        let VAL++
    done < $FILE_WITH_LIST
done

echo "Done!"
exit 0
