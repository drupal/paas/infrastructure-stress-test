import time, random, os, scrapy, requests, logging
from locust import HttpUser, task, between
from urllib.parse import urljoin
from bs4 import BeautifulSoup

logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(message)s',
    level=logging.INFO)

class Crawler(HttpUser):
    # If we want to have random intervals between requests
    # wait_time = between(0.5, 10)
    def __init__(self, *args, **kwargs):
        super(Crawler, self).__init__(*args, **kwargs)
        self.visited_urls = []
        self.urls_to_visit = [os.environ['URL']]
        self.original_link = os.environ['URL']

    def download_url(self, url):
        return self.client.get(url).text

    def get_linked_urls(self, url, html):
        soup = BeautifulSoup(html, 'html.parser')
        for link in soup.find_all('a'):
            path = link.get('href')
            if url.startswith(self.original_link) :
                if path and not path.startswith('//') and (path.startswith('/') or path.startswith('?')):
                    path = urljoin(url, path)
                    yield path

    def add_url_to_visit(self, url):
        if url not in self.visited_urls and url not in self.urls_to_visit:
            self.urls_to_visit.append(url)

    def crawl(self, url):
        html = self.download_url(url)
        for url in self.get_linked_urls(url, html):
            self.add_url_to_visit(url)

    def runCrawl(self):
        if len(self.urls_to_visit) > 0:
            url = self.urls_to_visit.pop(0)
            self.visited_urls.append(url)
            logging.info(f'Crawling: {url}')
            if url is None or url == "" or url.find("http") == -1:
               return 
            try:
                self.crawl(url)
            except Exception:
                logging.exception(f'Failed to crawl: {url}')
        else:
            self.client.get(self.visited_urls[random.randint(0,len(self.visited_urls)-1)])

    @task
    def test_run(self):
        self.runCrawl()

